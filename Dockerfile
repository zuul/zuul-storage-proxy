FROM docker.io/opendevorg/python-builder as builder

COPY . /tmp/src
RUN assemble

FROM docker.io/opendevorg/uwsgi-base as zuul-storage-proxy

COPY --from=builder /output/ /output
RUN /output/install-from-bindep \
  && useradd -u 10001 -m -d /var/lib/zuul -c "Zuul Daemon" zuul

EXPOSE 8000
ENV UWSGI_HTTP_SOCKET=:8000 UWSGI_PROCESSES=10 UWSGI_THREADS=1 MOUNTPOINT=/
CMD uwsgi --mount $MOUNTPOINT=zuul_storage_proxy:proxy --manage-script-name
